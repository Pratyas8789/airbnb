import './App.css';
import AllCards from './components/AllCards';
import Gallery from './components/Gallery';
import Navbar from './components/Navbar';

function App() {
  return (
    <div className="App">
      <Navbar/>
      <Gallery/>
      <AllCards/>
    </div>
  );
}
export default App;
