import React from 'react'

export default function (props) {
    let badeText
    if(props.openSpots===0){
        badeText="SOLD OUT"
    }else if(props.location==="Online"){
        badeText="ONLINE"
    }
    return (
        <div className="cards">
            <div className="pic">
                {badeText && <div className="text"> <p>{badeText}</p></div>}
                <img src={props.img} alt="" srcset="" />
            </div>
            <div className="about">
                <div className="rating">
                    <img className='star' src={props.star} alt="" srcset="" />
                    <span>{props.stats.rating}</span>
                    <span className="gray">{props.stats.countWithCountry}</span>
                </div>
                <p>{props.title}</p>
                <p><span className="bold">{props.price}</span> / person</p>
            </div>
        </div>

    )
}
