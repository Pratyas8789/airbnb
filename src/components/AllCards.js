import React from 'react'
import Cards from './Cards'
import Data from './Data'

export default function AllCards() {
    const AllCards = Data.map((details) => {
        return <Cards
            key={details.key}
            {...details}
        />
    })
    return (
        <div className='allCards'>
            {AllCards}
        </div>
    )
}
