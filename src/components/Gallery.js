import React from 'react'

export default function Gallery() {
  return (
    <div className='gallery'>
        <img src="./allPic/photo-grid.png" alt="" srcset="" />
        <h1>Online Experiences </h1>
        <p>Join unique interactive activities led by one-of-a-kind hosts—all without leaving home. </p>
    </div>
  )
}
